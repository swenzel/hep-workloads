# Changelog for IGWN pe

## [Unreleased]

## [v0.5] 2023-11-13
- Limit the number of loaded cores using the argument -n | --ncores 

## [v0.4] 2022-11-19
- Rebuild with the new CI/CD pipelines

## [v0.3] 2022-04-21
- increase version number

## [v0.2] 2022-04-09
- Enforce check of hardcoded params

## [v0.1] 2022-04-07
- First working release of IGWN pe
