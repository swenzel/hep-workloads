#!/bin/bash

# Copyright 2019-2022 CERN. See the COPYRIGHT file at the top-level
# directory of this distribution. For licensing information, see the
# COPYING file at the top-level directory of this distribution.

#set -x # enable debug printouts

#set -e # immediate exit on error

# Function doOne must be defined in each benchmark
# Input argument $1: process index (between 1 and $NCOPIES)
# Return value: please return 0 if this workload copy was successful, 1 otherwise
# The following variables are guaranteed to be defined and exported: NCOPIES, NTHREADS, NEVENTS_THREAD, EXTRA_ARGS, BMKDIR, DEBUG
# The function is started in process-specific working directory <basewdir>/proc_$1:
# please store here the individual log files for each of the NCOPIES processes
function doOne(){
  if [ "$1" == "" ] || [ "$2" != "" ]; then echo "[doOne] INTERNAL ERROR! Invalid arguments '$@' to doOne"; return 1; fi
  echo "[doOne ($1)] $(date) starting in $(pwd)"
  workDir=$(pwd)
  # Choose whether to run benchmarks on CPU only, GPU only or both (BMK-1018 and BMK-1017)
  # Based on EXTRA_ARGS passed via '--extra-args' (BMK-1014 and BMK-1031)
  echo "[doOne ($1)] $(date) EXTRA_ARGS='$EXTRA_ARGS'"
  status=0
  cpuorgpus=
  processes=
  flts=
  inls=
  bmkexeargs=
  for arg in $EXTRA_ARGS; do
    if [ "${arg}" == "--cpu" ] && [ "${cpuorgpus}" == "" ]; then
      cpuorgpus=${arg}
    elif [ "${arg}" == "--gpu" ] && [ "${cpuorgpus}" == "" ]; then
      cpuorgpus=${arg}
    elif [ "${arg}" == "--both" ] && [ "${cpuorgpus}" == "" ]; then
      cpuorgpus=${arg}
    elif [ "${arg}" == "-eemumu" ] && [ "${processes/eemumu }" == "${processes}" ]; then
      processes="${arg} ${processes}"
    elif [ "${arg}" == "-ggtt" ] && [ "${processes/ggtt }" == "${processes}" ]; then # NB: check for a space after -ggtt
      processes="${arg} ${processes}" # NB: always leave a space after -ggtt (and similarly for ggttg* below)
    elif [ "${arg}" == "-ggttg" ] && [ "${processes/ggttg }" == "${processes}" ]; then
      processes="${arg} ${processes}"
    elif [ "${arg}" == "-ggttgg" ] && [ "${processes/ggttgg }" == "${processes}" ]; then
      processes="${arg} ${processes}"
    ###elif [ "${arg}" == "-ggttggg" ] && [ "${processes/ggttggg }" == "${processes}" ]; then
    ###  processes="${arg} ${processes}" # CI build of ggttggg fails with "nvcc error: 'ptxas' died due to signal 9" (BMK-1036)
    elif [ "${arg}" == "-dbl" ] && [ "${flts/dbl }" == "${flts}" ]; then
      flts="${arg} ${flts}"
    elif [ "${arg}" == "-flt" ] && [ "${flts/flt }" == "${flts}" ]; then
      flts="${arg} ${flts}"
    elif [ "${arg}" == "-inl0" ] && [ "${inls/inl0 }" == "${inls}" ]; then
      inls="${arg} ${inls}"
    elif [ "${arg}" == "-inl1" ] && [ "${inls/inl1 }" == "${inls}" ]; then
      inls="${arg} ${inls}"
    elif [ "${arg#-p}" != "${arg}" ]; then
      arg123=${arg#-p}; arg1=${arg123%%,*}; arg23=${arg123#*,}; arg2=${arg23%%,*}; arg3=${arg23#*,}
      ###echo "arg1='$arg1' arg2='$arg2' arg3='$arg3'" # FOR DEBUGGING
      if [ "${arg1}" == "${arg123}" ] || [[ ${arg1} =~ [^0-9] ]] ||\
         [ "${arg2}" == "${arg23}" ] || [[ ${arg2} =~ [^0-9] ]] ||\
         [ "${arg3}" == "${arg23}" ] || [[ ${arg3} =~ [^0-9] ]]; then
        echo "[doOne ($1)] $(date) ERROR! Invalid argument '${arg}'"
        echo "[doOne ($1)] $(date) Usage: --extra-args '[--cpu|--gpu|-both] [-eemumu][-ggtt][-ggttg][-ggttgg] [-dbl][-flt] [-inl0][-inl1] [-p<nblks>,<nthrs>,<niter>]'"
        status=1; echo "[doOne ($1)] $(date) completed (status=${status})"; return ${status}
      fi
      bmkexeargs="-p ${arg1} ${arg2} ${arg3}"
    else
      echo "[doOne ($1)] $(date) ERROR! Invalid argument '${arg}'"
      echo "[doOne ($1)] $(date) Usage: --extra-args '[--cpu|--gpu|-both] [-eemumu][-ggtt][-ggttg][-ggttgg] [-dbl][-flt] [-inl0][-inl1] [-p<nblks>,<nthrs>,<niter>]'"
      status=1; echo "[doOne ($1)] $(date) completed (status=${status})"; return ${status}
    fi
  done
  if [ "${cpuorgpus}" == "" ]; then
    cpuorgpus="--both" # default is both CPU and GPU benchmarks
    echo "[doOne ($1)] $(date) No --cpu, --gpu or --both was specified in EXTRA_ARGS: run default cpuorgpus='--both' (CPU and GPU benchmarks)"
  else
    echo "[doOne ($1)] $(date) From EXTRA_ARGS: cpuorgpus='${cpuorgpus}'"
  fi
  if [ "${processes}" == "" ]; then
    processes="-ggttgg" # default is ggttgg only (BMK-1037)
    echo "[doOne ($1)] $(date) No -eemumu, -ggtt, -ggttg or -ggttgg was specified in EXTRA_ARGS: run default process='-ggttgg'"
  else
    processesold="${processes}"; processes=""; for process in ${processesold}; do processes="${process} ${processes}"; done # reverse list
    echo "[doOne ($1)] $(date) From EXTRA_ARGS: processes='${processes}'"
  fi
  if [ "${flts}" == "" ]; then
    echo "[doOne ($1)] $(date) No -dbl or -flt was specified in EXTRA_ARGS: run both double and single precision benchmarks"
    flts="-dbl -flt" # default is both dbl and flt (BMK-1037)
  else
    fltsold="${flts}"; flts=""; for flt in ${fltsold}; do flts="${flt} ${flts}"; done # reverse list
    echo "[doOne ($1)] $(date) From EXTRA_ARGS: flts='${flts}'"
  fi
  if [ "${inls}" == "" ]; then
    echo "[doOne ($1)] $(date) No -inl0 or -inl1 was specified in EXTRA_ARGS: run only benchmarks without inlining"
    inls="-inl0" # default is inl0 only (BMK-1037)
  else
    inlsold="${inls}"; inls=""; for inl in ${inlsold}; do inls="${inl} ${inls}"; done # reverse list
    echo "[doOne ($1)] $(date) From EXTRA_ARGS: inls='${inls}'"
  fi
  if [ "${bmkexeargs}" == "" ]; then
    echo "[doOne ($1)] $(date) No -p<nblks>,<nthrs>,<niter> was specified in EXTRA_ARGS: use process-dependent predefined numbers of events and 'GPU grids'"
  else
    echo "[doOne ($1)] $(date) From EXTRA_ARGS: BMKEXEARGS='${bmkexeargs}'"
  fi
  # Configure GPU tests if needed
  if [ "${cpuorgpus}" == "--cpu" ]; then
    cudaavxs="-noneonly -sse4only -avx2only -512yonly -512zonly" # equivalent to "-avxall -nocuda" (one action at a time)
    echo "[doOne ($1)] $(date) cpuorgpus=${cpuorgpus}: run only CPU tests"
  else
    if [ "${cpuorgpus}" == "--gpu" ]; then
      echo "[doOne ($1)] $(date) cpuorgpus=${cpuorgpus}: run only GPU tests"
      cudaavxs="-nocpp" # equivalent to "-nocpp" (one action at a time)
    else
      echo "[doOne ($1)] $(date) cpuorgpus=${cpuorgpus}: run both CPU and GPU tests"
      cudaavxs="-nocpp -noneonly -sse4only -avx2only -512yonly -512zonly" # equivalent to "-avxall" (one action at a time)
    fi
    # Test if a GPU exists (BMK-983)
    # [NB: if a GPU physically exists but nvidia-smi fails, then /dev will still contain nvidia*]
    # See https://docs.nvidia.com/datacenter/tesla/mig-user-guide/#device-nodes
    if ls /dev | grep nvidia > /dev/null; then
      echo "[doOne ($1)] $(date) cpuorgpus=${cpuorgpus} and a GPU is installed on this system (/dev/nvidia* found)"
      if [ "$NCOPIES" != "1" ]; then
        # Issue a warning not an error: the GPU can be shared across many processes/copies, but throughput is slower, eg lose 10% on 4 copies (BMK-1044)
        echo "[doOne ($1)] $(date) WARNING! NCOPIES=$NCOPIES slows down madgraph4gpu 'sa' benchmarks on one GPU: a single copy ('-c 1') is recommended"
      fi
      # Test access to the GPU
      # [NB: this will always be tested in the CI (assuming the GPU is correctly exposed via docker)
 #     if ! nvidia-smi; then
 #       echo "[doOne ($1)] $(date) ERROR! nvidia-smi failed: cannot access the GPU"
 #       status=1; echo "[doOne ($1)] $(date) completed (status=${status})"; return ${status}
 #     fi
    else
      echo "[doOne ($1)] $(date) WARNING! cpuorgpus=${cpuorgpus} but no GPU is installed on this system (no /dev/nvidia* found)"
      if [ "${cpuorgpus}" == "--both" ]; then
        echo "[doOne ($1)] $(date) WARNING! cpuorgpus=${cpuorgpus} but there is no GPU: run only the CPU benchmarks"
        cudaavxs="-noneonly -sse4only -avx2only -512yonly -512zonly" # equivalent to "-avxall -nocuda" (one action at a time)
      else
        echo "[doOne ($1)] $(date) ERROR! cpuorgpus=${cpuorgpus} but there is no GPU: there is no benchmark to run"
        status=1; echo "[doOne ($1)] $(date) completed (status=${status})"; return ${status}
      fi
    fi
    # Set up CUDA
    export CUDA_HOME=/usr/local/cuda-11.7
    export PATH=${CUDA_HOME}/bin:${PATH}
  fi
  # Configure WL copy
  download=0
  if [ ! -d /bmk/build/madgraph4gpu ]; then
    echo "[doOne ($1)] $(date) madgraph4gpu not found in /bmk/build/madgraph4gpu"
    echo "[doOne ($1)] $(date) download madgraph4gpu from github into ${workDir}/madgraph4gpu"
    # Download madgraph4gpu if not yet done
    # NB: this is executed only in the first CI docker run with network access (BMK-779)
    download=1
    # Checkout commit: 25 September 2022 within branch hack for PR #532
    git clone https://github.com/madgraph5/madgraph4gpu.git ${workDir}/madgraph4gpu
    cd ${workDir}/madgraph4gpu
    git reset --hard b40edd298171e08b5f52341d66adf2d9d70f4f5b
    # Implement here any custom changes to upstream madgraph4gpu as this becomes read-only in /bmk later on (BMK-1046)
    cd ${workDir}/madgraph4gpu/epochX/cudacpp/tput
    cp ./throughputX.sh ./throughputX.sh.old # KEEP IT IN THE IMAGE FOR REFERENCE
    echo "[doOne ($1)] $(date) Use \$BMKEXEARGS (if set) to replace process-dependent predefined numbers of events and 'GPU grids'"
    cat ./throughputX.sh | sed -e "s/BMKEXEARGS=\"\"/BMKEXEARGS=\"\${bmkexeargs}\"/" > ./throughputX.sh.new
    \mv ./throughputX.sh.new ./throughputX.sh
    echo "[doOne ($1)] $(date) Reinterpret \$NEVENTS_THREAD as a multiplier (BMKMULTIPLIER) for process-dependent predefined numbers of events"
    cat ./throughputX.sh | sed -e "s/BMKMULTIPLIER=1/BMKMULTIPLIER=\${NEVENTS_THREAD}/" > ./throughputX.sh.new
    \mv ./throughputX.sh.new ./throughputX.sh
    chmod +x ./throughputX.sh
    ###diff ./throughputX.sh.old ./throughputX.sh # FOR DEBUGGING
  else
    echo "[doOne ($1)] $(date) madgraph4gpu found in /bmk/build/madgraph4gpu, no need to download it from github"
    echo "[doOne ($1)] $(date) copy /bmk/build/madgraph4gpu into ${workDir}/madgraph4gpu"
    ###cp -dpr /bmk/build/madgraph4gpu ${workDir}/madgraph4gpu # unnecessary, no need to modify madgraph4gpu (BMK-1046)
    ln -sf /bmk/build/madgraph4gpu ${workDir}/madgraph4gpu # faster than copying (BMK-1046)
  fi
  ###echo "[doOne ($1)] $(date) dump contents of ${workDir}/madgraph4gpu: START" # FOR DEBUGGING
  ###find ${workDir}/madgraph4gpu -maxdepth 3 | sort                             # FOR DEBUGGING
  ###echo "[doOne ($1)] $(date) dump contents of ${workDir}/madgraph4gpu: END"   # FOR DEBUGGING
  # Setup WL copy
  cd ${workDir}/madgraph4gpu/epochX/cudacpp/tput
  makej=-makej # use 'make -j' parallel builds in the CI (note: disabling this did not fix ggttggg CI builds BMK-1036)
  
  # BMK-1294 For Alma9 use out-of-the-box gcc, commenting the following lines
  # source /cvmfs/sft.cern.ch/lcg/releases/gcc/11.2.0-ad950/x86_64-centos7/setup.sh
  # export PATH=/cvmfs/sft.cern.ch/lcg/releases/ccache/4.3-ed8d3/x86_64-centos7-gcc8-opt/bin:$PATH # NB needs also gcc11.2 above

  if [ "$CCACHE_DIR" != "" ] && [ -d $CCACHE_DIR ] && ccache -M 10G; then export USECCACHE=1; else export USECCACHE=0; fi # BMK-1030
  echo "USECCACHE=${USECCACHE} CCACHE_DIR=${CCACHE_DIR}"
  
  # BMK-1294 For Alma9 use out-of-the-box gcc, commenting the following lines
  # export PATH=/cvmfs/sft.cern.ch/lcg/releases/CMake/3.18.4-2ffec/x86_64-centos7-gcc8-opt/bin:$PATH

  if [ "${bmkexeargs}" != "" ]; then
    echo "[doOne ($1)] $(date) BMKEXEARGS='${bmkexeargs}' is set: replace process-dependent predefined numbers of events and 'GPU grids'"
    export bmkexeargs # bug fix: make this visible in throughputX.sh
  fi
  echo "[doOne ($1)] $(date) Reinterpret \$NEVENTS_THREAD=${NEVENTS_THREAD} as a multiplier (BMKMULTIPLIER) for process-dependent predefined numbers of events"
  # Build the code for many NVidia GPU architectures: 60/P100, 70/V100 (default), 80/A100 (see BMK-1060)
  export MADGRAPH_CUDA_ARCHITECTURE=60,70,80
  # Execute sub-actions of WL copy one by one
  for process in $processes; do
    ###if [ "$status" != "0" ]; then break; fi # breaks the for-loop over cudaavxs
    for cudaavx in $cudaavxs; do
      ###if [ "$status" != "0" ]; then break; fi # breaks the for-loop over cudaavxs
      for flt in $flts; do
        ###if [ "$status" != "0" ]; then break; fi # breaks the for-loop over flts
        if [ "${flt}" == "-dbl" ]; then flt2=""; else flt2="-fltonly"; fi # convert to weird throughputX.sh options
        for inl in $inls; do
          if [ "${inl}" == "-inl0" ]; then inl2=""; else inl2="-inlonly"; fi # convert to weird throughputX.sh options
          # Sleep at most for 1000 steps of 0.1 seconds each, i.e. 100 seconds in total
          syncprocesses $1 --workDir $workDir --status $status --sleepstep 0.1 --sleepstepsleft 1000
          status=${?}
          # Execute one action
          action="${process} ${cudaavx} ${flt2} ${inl2}"
          if [ "$status" != "0" ]; then
            # This process failed setup or a previous action, or it received ABORT from a failure in another process 
            echo "[doOne ($1)] $(date) current status=$status: skip next action '${action}'"
            ###break # breaks the for-loop over inls
          else
            echo "[doOne ($1)] $(date) current status=$status: execute next action '${action}'"
            echo "[doOne ($1)] $(date) Will execute './throughputX.sh ${makej} ${action}' from $(pwd)"
            echo "[doOne ($1)] $(date) Will execute './throughputX.sh ${makej} ${action}' from $(pwd)" \
              >> ${workDir}/out_$1.log 2> >(tee -a ${workDir}/out_$1.log >&2)
            ./throughputX.sh ${makej} ${action} \
              >> ${workDir}/out_$1.log 2> >(tee -a ${workDir}/out_$1.log >&2)
            status=${?}
            echo "[doOne ($1)] $(date) Have executed these runExe:"
            ./throughputX.sh ${makej} ${action} -dryrun | egrep '(^On|^runExe)'
            echo "[doOne ($1)] $(date) current status=$status: completed action '${action}'"
          fi
        done
      done
    done
  done
  # Copy madgraph4gpu including newly built binaries to ${workDir}/build (only in process 1!)
  # The bmk-driver.sh will then copy this to /results/build
  # This is the MAIN_BUILDEXPORTDIR directory from where main.sh then copies data into /bmk/build
  # NB: this is executed only in the first CI docker run with network access (BMK-779)
  if [ "${status}" == "0" ] && [ "$1" == "1" ]; then
    if [ "${download}" == "1" ]; then
      echo "[doOne ($1)] $(date) copy ${workDir}/madgraph4gpu to ${workDir}/build/madgraph4gpu"
      mkdir ${workDir}/build
      cp -dpr ${workDir}/madgraph4gpu ${workDir}/build/madgraph4gpu
    else
      echo "[doOne ($1)] $(date) madgraph4gpu already exists in /bmk/build/madgraph4gpu"
    fi
  fi
  # Clean up
  \rm -rf ${workDir}/madgraph4gpu
  echo "[doOne ($1)] $(date) completed (status=${status})"
  # Return 0 if this workload copy was successful, 1 otherwise
  return ${status}
}

# Optional function usage_detailed may be defined in each benchmark
# Input arguments: none
# Return value: none
function usage_detailed(){
  echo "Optional EXTRA_ARGS: [--cpu|--gpu|-both] [-eemumu][-ggtt][-ggttg][-ggttgg] [-dbl][-flt] [-inl0][-inl1] [-p<nblks>,<nthrs>,<niter>]"
  echo "  --cpu   : (CPU or GPU?)     run only the C++ benchmarks on CPU (1 or more copies)"
  echo "  --gpu   : (CPU or GPU?)     run only the CUDA benchmarks on GPU (1 copy)"
  echo "  --both  : (CPU or GPU?)     run both the C++ benchamrks on CPU and the CUDA benchmarks on GPU (1 copy)"
  echo "  -eemumu : (physics process) e+ e- --> mu+mu- (LEP, low computational intensity, high overhead from memory access/copy)"
  echo "  -ggtt   : (physics process) g g --> top top~ (LHC, low computational intensity, high overhead from memory access/copy)"
  echo "  -ggttg  : (physics process) g g --> top top~ g (LHC, higher computational intensity)"
  echo "  -ggttgg : (physics process) g g --> top top~ g g (LHC, even higher computational intensity)"
  echo "  -dbl    : (float precision) double precision 'd' benchmarks"
  echo "  -flt    : (float precision) single precision 'f' benchmarks"
  echo "  -inl0   : (C++ inlining)    benchmarks without C++ aggressive inlining"
  echo "  -inl1   : (C++ inlining)    benchmarks with C++ aggressive inlining"
  echo "  -p      : (!EXPERT MODE!)   '-p<nblks>,<nthrs>,<niter>' resets check/gcheck.exe arguments to '-p <nblks> <nthrs> <niter>' (NB: <nblks> and <nthrs> must be multiples of 2)"
  echo "DEFAULT is '--both -ggttgg -dbl -flt -inl0' (both CPU and GPU, ggttgg only, both d and f, inl0 only)" # see BMK-1037
  echo ""
  echo "If you start the benchmark by mistake and need to kill all associated processes, run the following command:"
  echo "  kill \$(ps -ef | egrep '(mg5amc-madgraph4gpu-2022-bmk.sh|throughputX.sh|check.exe)' | grep -v grep | awk '{print \$2}')"
}

# Default values for NCOPIES, NTHREADS, NEVENTS_THREAD must be set in each benchmark
NCOPIES=$(nproc)
NTHREADS=1 # cannot be changed by user input (single-threaded single-process WL)
NEVENTS_THREAD=1 # multiplier for a process-dependent predefined number of events

# Source the common benchmark driver
if [ -f $(dirname $0)/bmk-driver.sh ]; then
  . $(dirname $0)/bmk-driver.sh
else
  . $(dirname $0)/../../../common/bmk-driver.sh
fi
