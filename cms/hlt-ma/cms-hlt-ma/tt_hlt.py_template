
# Auto generated configuration file
# using:
# Revision: 1.19
# Source: /local/reps/CMSSW/CMSSW/Configuration/Applications/python/ConfigBuilder.py,v
# with command line options: tt_hlt --conditions auto:phase1_2022_realistic -n 1000 --era Run3 --eventcontent RAWSIM --procModifiers premix_stage2 --filein file:tt_digi_DIGI_DATAMIX_L1_DIGI2RAW.root -s RAW2DIGI:RawToDigi_pixelOnly,RECO:reconstruction_pixelTrackingOnly --datatier GEN-SIM-DIGI-RAW --geometry DB:Extended --nThreads 8 --suffix -j JobReport5.xml --customise Validation/Performance/TimeMemorySummary.customiseWithTimeMemorySummary --procModifiers pixelNtupletFit,gpu --customise RecoPixelVertexing/Configuration/customizePixelTracksForTriplets.customizePixelTracksForTriplets --customise RecoTracker/Configuration/customizePixelOnlyForProfiling.customizePixelOnlyForProfilingGPUWithHostCopy
import FWCore.ParameterSet.Config as cms

from Configuration.Eras.Era_Run3_cff import Run3
from Configuration.ProcessModifiers.premix_stage2_cff import premix_stage2
from Configuration.ProcessModifiers.pixelNtupletFit_cff import pixelNtupletFit
from Configuration.ProcessModifiers.gpu_cff import gpu

process = cms.Process('RECO',Run3,premix_stage2,pixelNtupletFit,gpu)

# import of standard configurations
process.load('Configuration.StandardSequences.Services_cff')
process.load('SimGeneral.HepPDTESSource.pythiapdt_cfi')
process.load('FWCore.MessageService.MessageLogger_cfi')
process.load('Configuration.EventContent.EventContent_cff')
process.load('SimGeneral.MixingModule.mixNoPU_cfi')
process.load('Configuration.StandardSequences.GeometryRecoDB_cff')
process.load('Configuration.StandardSequences.MagneticField_cff')
process.load('Configuration.StandardSequences.RawToDigi_cff')
process.load('Configuration.StandardSequences.Reconstruction_cff')
process.load('Configuration.StandardSequences.EndOfProcess_cff')
process.load('Configuration.StandardSequences.FrontierConditions_GlobalTag_cff')

process.maxEvents = cms.untracked.PSet(
    input = cms.untracked.int32(_NEVENTS_),
    output = cms.optional.untracked.allowed(cms.int32,cms.PSet)
)

# Input source


process.source = cms.Source("RepeatingCachedRootSource",
     fileName = cms.untracked.string('file:digi.root'),
     repeatNEvents = cms.untracked.uint32(100)
)

process.options = cms.untracked.PSet(
    FailPath = cms.untracked.vstring(),
    IgnoreCompletely = cms.untracked.vstring(),
    Rethrow = cms.untracked.vstring(),
    SkipEvent = cms.untracked.vstring(),
    accelerators = cms.untracked.vstring('*'),
    allowUnscheduled = cms.obsolete.untracked.bool,
    canDeleteEarly = cms.untracked.vstring(),
    deleteNonConsumedUnscheduledModules = cms.untracked.bool(True),
    dumpOptions = cms.untracked.bool(False),
    emptyRunLumiMode = cms.obsolete.untracked.string,
    eventSetup = cms.untracked.PSet(
        forceNumberOfConcurrentIOVs = cms.untracked.PSet(
            allowAnyLabel_=cms.required.untracked.uint32
        ),
        numberOfConcurrentIOVs = cms.untracked.uint32(0)
    ),
    fileMode = cms.untracked.string('FULLMERGE'),
    forceEventSetupCacheClearOnNewRun = cms.untracked.bool(False),
    makeTriggerResults = cms.obsolete.untracked.bool,
    numberOfConcurrentLuminosityBlocks = cms.untracked.uint32(0),
    numberOfConcurrentRuns = cms.untracked.uint32(1),
    numberOfStreams = cms.untracked.uint32(0),
    numberOfThreads = cms.untracked.uint32(1),
    printDependencies = cms.untracked.bool(False),
    sizeOfStackForThreadsInKB = cms.optional.untracked.uint32,
    throwIfIllegalParameter = cms.untracked.bool(True),
    wantSummary = cms.untracked.bool(False)
)

# Production Info
process.configurationMetadata = cms.untracked.PSet(
    annotation = cms.untracked.string('tt_hlt nevts:_NEVENTS_'),
    name = cms.untracked.string('Applications'),
    version = cms.untracked.string(': 1.19 $')
)

# Output definition

process.RAWSIMoutput = cms.OutputModule("PoolOutputModule",
    compressionAlgorithm = cms.untracked.string('LZMA'),
    compressionLevel = cms.untracked.int32(1),
    dataset = cms.untracked.PSet(
        dataTier = cms.untracked.string('GEN-SIM-DIGI-RAW'),
        filterName = cms.untracked.string('')
    ),
    eventAutoFlushCompressedSize = cms.untracked.int32(20971520),
    fileName = cms.untracked.string('tt_hlt_RAW2DIGI_RECO.root'),
    outputCommands = process.RAWSIMEventContent.outputCommands,
    splitLevel = cms.untracked.int32(0)
)

# Additional output definition

# Other statements
from Configuration.AlCa.GlobalTag import GlobalTag
process.GlobalTag = GlobalTag(process.GlobalTag, 'auto:phase1_2022_realistic', '')
process.GlobalTag.connect = 'sqlite_file:./GlobalTag.db'
process.GlobalTag.DumpStat = True
process.GlobalTag.snapshotTime = cms.string('9999-12-31 23:59:59.000')

# Path and EndPath definitions
process.raw2digi_step = cms.Path(process.RawToDigi_pixelOnly)
process.reconstruction_step = cms.Path(process.reconstruction_pixelTrackingOnly)
process.endjob_step = cms.EndPath(process.endOfProcess)
process.RAWSIMoutput_step = cms.EndPath(process.RAWSIMoutput)

# Schedule definition
process.schedule = cms.Schedule(process.raw2digi_step,process.reconstruction_step,process.endjob_step,process.RAWSIMoutput_step)
from PhysicsTools.PatAlgos.tools.helpers import associatePatAlgosToolsTask
associatePatAlgosToolsTask(process)

#Setup FWK for multithreaded
process.options.numberOfThreads = _NTHREADS_
process.options.numberOfStreams = 0

# customisation of the process.

# Automatic addition of the customisation function from Validation.Performance.TimeMemorySummary
from Validation.Performance.TimeMemorySummary import customiseWithTimeMemorySummary

#call to customisation function customiseWithTimeMemorySummary imported from Validation.Performance.TimeMemorySummary
process = customiseWithTimeMemorySummary(process)

# Automatic addition of the customisation function from RecoPixelVertexing.Configuration.customizePixelTracksForTriplets
from RecoPixelVertexing.Configuration.customizePixelTracksForTriplets import customizePixelTracksForTriplets

#call to customisation function customizePixelTracksForTriplets imported from RecoPixelVertexing.Configuration.customizePixelTracksForTriplets
process = customizePixelTracksForTriplets(process)

# Automatic addition of the customisation function from RecoTracker.Configuration.customizePixelOnlyForProfiling
from RecoTracker.Configuration.customizePixelOnlyForProfiling import customizePixelOnlyForProfilingGPUWithHostCopy

#call to customisation function customizePixelOnlyForProfilingGPUWithHostCopy imported from RecoTracker.Configuration.customizePixelOnlyForProfiling
process = customizePixelOnlyForProfilingGPUWithHostCopy(process)

# End of customisation functions


# Customisation from command line

#Have logErrorHarvester wait for the same EDProducers to finish as those providing data for the OutputModule
from FWCore.Modules.logErrorHarvester_cff import customiseLogErrorHarvesterUsingOutputCommands
process = customiseLogErrorHarvesterUsingOutputCommands(process)

# Add early deletion of temporary data products to reduce peak memory need
from Configuration.StandardSequences.earlyDeleteSettings_cff import customiseEarlyDelete
process = customiseEarlyDelete(process)
# End adding early deletion
