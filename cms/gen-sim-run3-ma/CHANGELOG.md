# Changelog for CMS gen-sim-run3-ma

## [Unreleased]

## [v1.1] 2023-06-01
- Limit the number of loaded cores using the argument -n | --ncores 

## [v1.0] 2022-10-06
- First working release of CMS gen-sim run3 for x86_64 and aarch64
- The version starts from v1.0 to avoid confusion with the legacy images built in cms/gen-sim-run3 having version v0.x
- This release is based on CMSSW_12_5_0 and is different w.r.t. the workload built in cms/gen-sim-run3 that is based on CMSSW_11_2_0
