#! /bin/sh
# DBRelease setup
echo Setting up DBRelease /cvmfs/atlas.cern.ch/repo/sw/database/DBRelease/300.0.4 environment
export DBRELEASE=300.0.4
export CORAL_AUTH_PATH=/cvmfs/atlas.cern.ch/repo/sw/database/DBRelease/300.0.4/XMLConfig
export CORAL_DBLOOKUP_PATH=/cvmfs/atlas.cern.ch/repo/sw/database/DBRelease/300.0.4/XMLConfig
export TNS_ADMIN=/cvmfs/atlas.cern.ch/repo/sw/database/DBRelease/300.0.4/oracle-admin
DATAPATH=/cvmfs/atlas.cern.ch/repo/sw/database/DBRelease/300.0.4:$DATAPATH
athena.py --threads=2 runargs.EVNTtoHITS.py SimuJobTransforms/skeleton.EVGENtoHIT_ISF.py
