# Changelog for Atlas gen_sherpa MultiArchitecture (MA)

## [Unreleased]

## [v2.1] 2023-11-10
- Limit the number of loaded cores using the argument -n | --ncores 

## [v2.0] 2022-10-13
- First working release of Atlas gen_sherpa built for x86_64 and aarch64
- The version starts from v2.0 to avoid confusion with the legacy images built in atlas/gen_sherpa
- This release is based on Athena 23.0.3 and is different w.r.t. the workload built in atlas/gen_sherpa that is based on Athena 21.6.84
