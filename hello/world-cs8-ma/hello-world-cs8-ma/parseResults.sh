# Copyright 2019-2020 CERN. See the COPYRIGHT file at the top-level
# directory of this distribution. For licensing information, see the
# COPYING file at the top-level directory of this distribution.

parseResultsDir=$(cd $(dirname ${BASH_SOURCE}); pwd) # needed to locate parseResults.py

# Function parseResults must be defined in each benchmark (or in a separate file parseResults.sh)
# The following variables are guaranteed to be defined and exported: NCOPIES, NTHREADS, NEVENTS_THREAD, BMKDIR, DEBUG, APP
# Logfiles have been stored in process-specific working directories <basewdir>/proc_<1...NCOPIES>
# The function is started in the base working directory <basewdir>:
# please store here the overall json summary file for all NCOPIES processes combined
function parseResults(){
  echo "[parseResults] current directory: $(pwd)"
  # #-----------------------
  # Parse results (bash)
  #-----------------------
  echo "[parseResults] bash parser starting"
  # Parsing  Event Throughput: xxxx ev/s
  resJSON=`echo '' | awk '{ 
      printf "\"wl-scores\": {\"gen\": %.4f} ,\"wl-stats\": {\"avg\": %.4f, \"median\": %.4f, \"min\": %.4f, \"max\": %.4f, \"count\": %d}", 
      ncopies, 1, 1, 1, 1, ncopies
    }' ncopies=$NCOPIES `
  shstatus=$?
  [ "$shstatus" != "0" ] && return $shstatus
  #-----------------------
  # Generate summary
  #-----------------------
  echo "[parseResults] generate report"
  echo "{$resJSON}" > $baseWDir/parser_output.json  
  #-----------------------
  # Return status
  #-----------------------
  return $shstatus
}
