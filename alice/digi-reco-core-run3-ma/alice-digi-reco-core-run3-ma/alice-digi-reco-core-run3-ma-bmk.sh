#!/bin/bash

# Copyright 2019-2020 CERN. See the COPYRIGHT file at the top-level
# directory of this distribution. For licensing information, see the
# COPYING file at the top-level directory of this distribution.

#set -x # enable debug printouts

#set -e # immediate exit on error

# random 4-byte number as base identifier for this benchmark
export ALIEN_PROC_ID_BASE=`od -An -N4 -tu4 < /dev/urandom`

# Function doOne must be defined in each benchmark
# Input argument $1: process index (between 1 and $NCOPIES)
# Return value: please return 0 if this workload copy was successful, 1 otherwise
# The following variables are guaranteed to be defined and exported: NCOPIES, NTHREADS, NEVENTS_THREAD, BMKDIR, DEBUG
# The function is started in process-specific working directory <basewdir>/proc_$1:
# please store here the individual log files for each of the NCOPIES processes
function doOne(){
  if [ "$1" == "" ] || [ "$2" != "" ]; then echo "[doOne] ERROR! Invalid arguments '$@' to doOne"; return 1; fi
  echo "[doOne ($1)] $(date) starting in $(pwd)"

  #
  # ALICE Run3 workflow doing digitization + core reconstruction steps.
  # Digitization performs event mixing/embedding (background events are reused across timeframes). 
  # 
  /cvmfs/alice.cern.ch/bin/alienv printenv O2/nightly-20221215-1 &> environment
  source environment

  # get the workflow generation script locally because we need to patch it for ARM
  export O2DPG_ROOT=/cvmfs/alice.cern.ch/el7-x86_64/Packages/O2DPG/nightly-20221215-1/
  cp ${O2DPG_ROOT}/MC/bin/o2dpg_sim_workflow.py .
  # we have a problem with Pandas on ARM (which is not needed in this benchmark)
  sed -i 's/import pandas/#import pandas/g' o2dpg_sim_workflow.py
  export PYTHONPATH=$PYTHONPATH:${O2DPG_ROOT}/MC/bin/
  
  export HEPSCORE_CCDB_ROOT=/cvmfs/alice.cern.ch/el7-x86_64/Packages/HEPscore-CCDB/v0.1.5-coredigireco-snapshot-compatible-20221215-1

  # make sure O2DPG + O2 is loaded
  [ ! "${O2DPG_ROOT}" ] && echo "Error: This needs O2DPG loaded" && return 1
  [ ! "${O2_ROOT}" ] && echo "Error: This needs O2 loaded" && return 1

  # set ALIEN_PROC_ID (it is used to make sockets in containers and across unique)
  export ALIEN_PROC_ID="${ALIEN_PROC_ID_BASE}$1" # append copy number to a base identifier
  echo "ALIEN_PROC_ID is ${ALIEN_PROC_ID}"

  # ----------- START ACTUAL JOB  ----------------------------- 
  NSIGEVENTS=$(($NEVENTS_THREAD * $NTHREADS))
  NBKGEVENTS=$NSIGEVENTS
  NTIMEFRAMES=2
  SIMENGINE=TGeant4 
  NWORKERS=$NTHREADS
  CPULIMIT=$NTHREADS
  MODULES="--skipModules ZDC"
  PYPROCESS=ccbar 

  # create workflow
  ./o2dpg_sim_workflow.py -eCM 5020 -col pp -gen pythia8 -proc ${PYPROCESS} \
                          -colBkg PbPb -genBkg pythia8 -procBkg "heavy_ion" \
                          -tf ${NTIMEFRAMES} -nb ${NBKGEVENTS}              \
                          -ns ${NSIGEVENTS} -e ${SIMENGINE}                 \
                          -j ${NWORKERS} --embedding -interactionRate 50000 \
                          -run 310000 -seed 1 --timestamp 1550600800001 > out_$1.log 2>&1

  # fetch and unpack checkpoint file containing all inputs (simulated hits)
  cp ${HEPSCORE_CCDB_ROOT}/hepscore_checkpoint.tar.gz .
  tar -xzf hepscore_checkpoint.tar.gz 
  rm hepscore_checkpoint.tar.gz
  rm ${PWD}/.ccdb/log
  export ALICEO2_CCDB_LOCALCACHE=${PWD}/.ccdb

  # whenever the number of timeframes > 2, we need to create/scale-up the GEANT hit
  # input files (because only  input for 2 timeframes is provided in the checkpoint)
  if [ $NTIMEFRAMES -gt 2 ]; then
    for tf_to in $(seq 3 ${NTIMEFRAMES}); do
      tf_from=$((1 + tf_to % 2))
      cp -R tf${tf_from} tf${tf_to}
      # fix filenames
      cd tf${tf_to}
      # renames all the files from timeframe ${tf_from} to timeframe ${tf_to}
      for file in *_${tf_from}*; do
        mv $file $(echo "$file" | sed "s/\(.*\)_${tf_from}\(.*\)/\1_${tf_to}\2/")
      done
      cd ..
    done
  fi

  # we use knowledge about the copy id of this benchmark and the total CPU count
  # to isolate/pin this benchmark invocation to a fixed set of CPUs
  COPY=$1
  let RANK=${COPY}-1
  let LOWERCPU=${RANK}*${NWORKERS}
  let UPPERCPU=${COPY}*${NWORKERS}-1  

  export OMP_NUM_THREADS=${NTHREADS}
  export SHMSIZE=${SHMSIZE:-8000000000}

  # construct runtime command and run                            
  COMMAND="${O2DPG_ROOT}/MC/bin/o2_dpg_workflow_runner.py -f workflow.json ${O2DPG_MAX_PARALLELTASKS_ARG:--j 2} --cpu-limit ${CPULIMIT} -tt trdreco --rerun-from digic --update-resources ${O2DPG_METRICS_FILE:-alternate_metrics.json} >> out_$1.log 2>&1"

  # optionally use taskset to constrain to physical CPU cores (only relevant when not using the full machine)
  [[ "${USETASKSET}" == "1" ]] && COMMAND="taskset -c ${LOWERCPU}-${UPPERCPU} ${COMMAND}"
  echo "${COMMAND}"
  eval ${COMMAND}
  status=${?}

  # Calculate some relative activity numbers per category (SIM, DIGI, RECO, AOD),
  # not really weighting according to internal multi-core behaviour, though.
  # Also, the sampling is rather crude and numbers only a rough estimate.
  TOTAL=`awk 'BEGIN{c=0} /iter/ { c+=1 } END {print 5*c}' pipeline_metric_*.log`
  RECOTIME=`awk 'BEGIN{c=0} /RECO/ { c+=1 } END {print 5*c}' pipeline_metric_*.log`
  DIGITIME=`awk 'BEGIN{c=0} /DIGI/ { c+=1 } END {print 5*c}' pipeline_metric_*.log`
  RECOFRACTION=$(awk -v "t=${TOTAL}" -v "r=${RECOTIME}" 'BEGIN { print (r/t); }')
  DIGIFRACTION=$(awk -v "t=${TOTAL}" -v "d=${DIGITIME}" 'BEGIN { print (d/t); }')
  echo "digi time ${DIGITIME}s; digi active fraction ~${DIGIFRACTION}%" >> out_$1.log
  echo "reco time ${RECOTIME}s; reco active fraction ~${RECOFRACTION}%" >> out_$1.log
  # Note that these timings will not add up to the total walltime of the workflow
  # due to an interleaving + parallel nature.

  echo "[doOne ($1)] $(date) completed (status=$status)"
  # Return 0 if this workload copy was successful, 1 otherwise
  return $status
}

# Optional function validateInputArguments may be defined in each benchmark
# If it exists, it is expected to set NCOPIES, NTHREADS, NEVENTS_THREAD
# (based on previous defaults and on user inputs USER_NCOPIES, USER_NTHREADS, USER_NEVENTS_THREADS)
# Input arguments: none
# Return value: please return 0 if input arguments are valid, 1 otherwise
# The following variables are guaranteed to be defined: NCOPIES, NTHREADS, NEVENTS_THREAD
# (benchmark defaults) and USER_NCOPIES, USER_NTHREADS, USER_NEVENTS_THREADS (user inputs)
function validateInputArguments(){
  if [ "$1" != "" ]; then echo "[validateInputArguments] ERROR! Invalid arguments '$@' to validateInputArguments"; return 1; fi
  echo "[validateInputArguments] validate input arguments"
  # Number of copies and number of threads per copy
  if [ "$USER_NTHREADS" != "" ] && [ "$USER_NCOPIES" != "" ]; then
    NCOPIES=$USER_NCOPIES
    NTHREADS=$USER_NTHREADS
  elif [ "$USER_NTHREADS" != "" ]; then
    NTHREADS=$USER_NTHREADS
    NCOPIES=$((`nproc`/$NTHREADS))
  elif [ "$USER_NCOPIES" != "" ]; then
    NCOPIES=$USER_NCOPIES
    NTHREADS=$((`nproc`/$NCOPIES))
  fi
  # Number of events per thread
  if [ "$USER_NEVENTS_THREAD" != "" ]; then NEVENTS_THREAD=$USER_NEVENTS_THREAD; fi

  USETASKSET=0
  for arg in $EXTRA_ARGS; do
    [ "${arg}" == "--taskset" ] && USETASKSET=1
  done 

  echo "[validateInputArguments] USETASKSET=${USETASKSET}"

  # Return 0 if input arguments are valid, 1 otherwise
  return 0 # no checks
}

# Default values for NCOPIES, NTHREADS, NEVENTS_THREAD must be set in each benchmark
NTHREADS=4 
NCOPIES=$(( `nproc` / $NTHREADS ))
NEVENTS_THREAD=3
if [ "$NCOPIES" -lt 1 ]; then # when $NTHREADS > nproc
  NCOPIES=1
  NTHREADS=`nproc`
fi
# Source the common benchmark driver
if [ -f $(dirname $0)/bmk-driver.sh ]; then
  . $(dirname $0)/bmk-driver.sh
else
  . $(dirname $0)/../../../common/bmk-driver.sh
fi
