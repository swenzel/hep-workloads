# Changelog for Juno gen-sim-reco

## [Unreleased]

## [v2.2] 2023-11-13
- Limit the number of loaded cores using the argument -n | --ncores

## [v2.1] 2022-11-19
- Rebuild with the new CI/CD pipelines

## [v2.0] 2022-07-01
- use elapsed time instead of user time as reported metric

## [v1.1] 2021-12-16
- Apply same parser base template to all workloads

## [v1.0] 2021-11-03
- First working release of Juno gen-sim-reco
