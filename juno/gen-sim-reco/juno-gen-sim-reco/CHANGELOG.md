# Changelog for Juno gen-sim-reco

## [Unreleased]

## [v2.0] 2022-07-01
- Modify the time report, using the walltime and not only the user time component of time (https://its.cern.ch/jira/browse/BMK-975)

## [v1.1] 2021-12-16
- First working release of Juno gen-sim-reco