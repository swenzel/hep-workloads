#!/bin/bash

#set -x # enable debug printouts

#set -e # immediate exit on error

# Function doOne must be defined in each benchmark
# Input argument $1: process index (between 1 and $NCOPIES)
# Return value: please return 0 if this workload copy was successful, 1 otherwise
# The following variables are guaranteed to be defined and exported: NCOPIES, NTHREADS, NEVENTS_THREAD, BMKDIR, DEBUG
# The function is started in process-specific working directory <basewdir>/proc_$1:
# please store here the individual log files for each of the NCOPIES processes
function doOne(){
  if [ "$1" == "" ] || [ "$2" != "" ]; then echo "[doOne] ERROR! Invalid arguments '$@' to doOne"; return 1; fi
  echo "[doOne ($1)] $(date) starting in $(pwd)"
  echo "[doOne ($1)] do one! (process $1 of $NCOPIES)"

  # Configure WL copy
  curdir=`pwd`
  echo "Current directory: $curdir"
  # Suppress unnecessary warning message
  export CMTCONFIG=amd64_linux26
  source /cvmfs/juno.ihep.ac.cn/centos7_amd64_gcc830/Pre-Release/J21v2r0-branch/setup.sh > /dev/null
  export CMTCONFIG=amd64_linux26
  source /cvmfs/juno.ihep.ac.cn/centos7_amd64_gcc830/Pre-Release/J21v2r0-branch/setup.sh > /dev/null
  # Execute WL copy
  echo "Executing the following number of threads:"
  echo $NTHREADS

  # Ignore requests for multi-threading
  echo "Start gen-sim:"
  /usr/bin/time -p -f %e -o outputsim.time python $TUTORIALROOT/share/tut_detsim.py --seed 10000 --evtmax ${NEVENTS_THREAD} --output detsim_10000_$1.root --user-output userdetsim_10000_$1.root gun > $curdir/outputsim
  echo "End gen-sim and Start Elec sim"
  /usr/bin/time -p -f %e -o outputdetele.time python $TUTORIALROOT/share/tut_det2elec.py --input  detsim_10000_$1.root --output det2elec_10000_$1.root --evtmax -1> $curdir/outputdetele
  echo "End Elec sim and start calb rec"
  /usr/bin/time -p -f %e -o outputelecal.time python $TUTORIALROOT/share/tut_elec2calib.py --no-conddb --input det2elec_10000_$1.root  --output elec2calib_10000_$1.root --evtmax -1 > $curdir/outputelecal
  echo "End calb rec and start envent rec"
  /usr/bin/time -p -f %e -o outputrec.time  python $TUTORIALROOT/share/tut_calib2rec.py --no-conddb --input  elec2calib_10000_$1.root --output calib2rec_10000_$1.root --evtmax -1> $curdir/outputrec
  status=$?
  echo "End event rec"

  # Set to 1 to print workload output to screen
  debug=0
  if [ $debug -eq 1 ]; then
    cd $curdir;
    echo "_________________________________________________OUTPUT START_________________________________________________";
    cat output;
    echo "_________________________________________________OUTPUT END_________________________________________________";
  fi 

  echo "[doOne ($1)] $(date) completed (status=$status)"
  # Return 0 if this workload copy was successful, 1 otherwise
  return $status
}

# Optional function validateInputArguments may be defined in each benchmark
# If it exists, it is expected to set NCOPIES, NTHREADS, NEVENTS_THREAD
# (based on previous defaults and on user inputs USER_NCOPIES, USER_NTHREADS, USER_NEVENTS_THREADS)
# Input arguments: none
# Return value: please return 0 if input arguments are valid, 1 otherwise
# The following variables are guaranteed to be defined: NCOPIES, NTHREADS, NEVENTS_THREAD
# (benchmark defaults) and USER_NCOPIES, USER_NTHREADS, USER_NEVENTS_THREADS (user inputs)
function validateInputArguments(){
  if [ "$1" != "" ]; then echo "[validateInputArguments] ERROR! Invalid arguments '$@' to validateInputArguments"; return 1; fi
  echo "[validateInputArguments] validate input arguments"
  # Dummy version: accept user inputs as they are
  if [ "$USER_NCOPIES" != "" ]; then NCOPIES=$USER_NCOPIES; fi
  if [ "$USER_NTHREADS" != "" ]; then NTHREADS=$USER_NTHREADS; fi
  if [ "$USER_NEVENTS_THREAD" != "" ]; then NEVENTS_THREAD=$USER_NEVENTS_THREAD; fi
  # Return 0 if input arguments are valid, 1 otherwise
  return 0
}

# Optional function usage_detailed may be defined in each benchmark
# Input arguments: none
# Return value: none
function usage_detailed(){
  echo "NCOPIES*NTHREADS may be lower or greater than nproc=$(nproc)"
}

# Default values for NCOPIES, NTHREADS, NEVENTS_THREAD must be set in each benchmark
NCOPIES=$(nproc)
NTHREADS=1 # cannot be changed by user input (single-threaded single-process WL)
NEVENTS_THREAD=50

# Source the common benchmark driver
if [ -f $(dirname $0)/bmk-driver.sh ]; then
  . $(dirname $0)/bmk-driver.sh
else
  . $(dirname $0)/../../../common/bmk-driver.sh
fi
