#!/bin/bash

# Copyright 2022-2023 CERN. See the COPYRIGHT file at the top-level
# directory of this distribution. For licensing information, see the
# COPYING file at the top-level directory of this distribution.

###
### The following set of utility functions
### is used in multiple CI jobs that are functional
### to the pipeline that executes build/test/publication/announcement
### of a WL image
### List of functions
### - Discover and setup CI env and WL env: source_specfile, find_specfile, 
### - Create temporary gitlab branch, open merge request, tag repository
### - Create multiarch docker manifest
### - Announce images
### - Test, build, publish Singularity images


function fail(){
    echo -e "\n------------------------\nFailing '$@'\n------------------------\n" >&2
    [ -e ${HEPWL_DOCKERIMAGENAME}.sif ] && rm -f ${HEPWL_DOCKERIMAGENAME}.sif
    echo -e "\n@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
    echo -e "\n[utility.sh] finished (NOT OK) at $(date)\n"
    echo -e "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n"
    exit 1
}


function execute_command_retry(){
    attempts=2
    attempt=0
    while [ $attempt -lt $attempts ]; 
        do
        eval "$@"
        status=$?
        echo -e "`date` [execute_command_retry] execution attempt $attempt of \n'$@'\nReturn code: ${status}"
        [[ $status -eq 0 ]] && break
        attempt=$((attempt+1))
    done
    return ${status}
}

### Wrapper function for backward compatibility with CI jobs calling .gitlab-ci.sh
function run_procedure(){
    echo -e "\n@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
    echo -e "\n[utility.sh] ${FUNCNAME[0]} starting at $(date)\n"
    echo -e "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n"
    echo "[utility.sh:run_procedure] list of HEPWL_BUILDARCH=${HEPWL_BUILDARCH}"
    echo "[utility.sh:run_procedure] This runner is on arch: $(uname -m)"
    if [[ ${HEPWL_BUILDARCH} =~ ($(uname -m)) ]]; then
        echo "[utility.sh:run_procedure] ARCH $(uname -m) found in HEPWL_BUILDARCH=${HEPWL_BUILDARCH}"
        echo "[utility.sh:run_procedure] CI_RUNNER_TAGS: ${CI_RUNNER_TAGS}"
        RUNNER_HAS_GPU=$(echo "${CI_RUNNER_TAGS}" | awk '{ if ($0~"gpu" && $0!~"no-gpu") {print 1} else {print 0}}')
        echo "[utility.sh:run_procedure] RUNNER_HAS_GPU=${RUNNER_HAS_GPU}"
        if [[ ${HEPWL_BMKUSEGPU} != "1" &&  "${RUNNER_HAS_GPU}" == "0" ]]; then
            echo "[utility.sh:run_procedure] HEPWL_BMKUSEGPU is != 1. Building only for CPU"
            echo "[utility.sh:run_procedure] Going to build image with JOBBUILDARG=$JOBBUILDARG"
            echo "[utility.sh:run_procedure] Going to build image with CIENV_BUILDIMG=$CIENV_BUILDIMG"
            export CIENV_BUILDIMG
            ./gitlab-ci.sh "$JOBBUILDARG"
        elif [[ ${HEPWL_BMKUSEGPU} == "1" &&  "${RUNNER_HAS_GPU}" == "1" ]]; then
            echo "[utility.sh:run_procedure] HEPWL_BMKUSEGPU is == 1 and Runner is tagged for GPU."
            echo "[utility.sh:run_procedure] Going to build image with JOBBUILDARG=$JOBBUILDARG"
            echo "[utility.sh:run_procedure] Going to build image with CIENV_BUILDIMG=$CIENV_BUILDIMG"
            export CIENV_BUILDIMG
            ./gitlab-ci.sh "$JOBBUILDARG"
        else
            echo "Runner tags and GPU requirement do not match"
            echo "Doing nothing"
        fi
    else
        echo "ARCH $(uname -m) NOT found in HEPWL_BUILDARCH=${HEPWL_BUILDARCH}"
        echo "Doing nothing"
    fi
    echo -e "\n@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
    echo -e "\n[utility.sh] ${FUNCNAME[0]} finished at $(date)\n"
    echo -e "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n"
}

##########################################################################
## block of functions to 
## Discover and setup CI env and WL env: source_specfile, find_specfile, 
##########################################################################

function source_specfile(){
    echo "[source_specfile] env variable CI_COMMIT_BRANCH=\"${CI_COMMIT_BRANCH}\""
    WLSPECpath=$(find_specfile)
    if [ "$?" != "0" ]; then "[source_specfile] find_specfile failed"; return 1; fi
    if [ "${WLSPECpath}" != "" ]; then
        echo "[source_specfile] Found spec file ${WLSPECpath}"
        cat "$WLSPECpath"
        echo ""
        source "$WLSPECpath"
    else
        echo "[source_specfile] Spec file not found for CI_COMMIT_BRANCH ${CI_COMMIT_BRANCH}."
        return 1
    fi
    # For backward compatibility force the x86_64 arch
    if [ "$HEPWL_BUILDARCH" == "" ];
        then
        HEPWL_BUILDARCH="x86_64"
        echo "[source_specfile] HEPWL_BUILDARCH variable not found, forcing to ${HEPWL_BUILDARCH}"
    fi
    return 0
}

# The spec file is found imposing that the build branch
# has name pattern qa-build-<name> where <name> is the name of the spec file
# It works for the current structure of all WL directories
function find_specfile(){
### CAVEAT: do not print anything in this function, but the echo "$WLSPECpath"
    WLSPEClocalpath=""
    WLname=${CI_COMMIT_BRANCH/qa-build-/}
    WLSPEClocalpath=$(find . -name "${WLname}.spec")
    if [ "${WLSPEClocalpath}" != "" ]; then
        WLSPECpath=$(readlink -f ${WLSPEClocalpath})
        returnStatus=0
    else
        WLSPECpath=""
        returnStatus=1
    fi
    echo "$WLSPECpath"
    return $returnStatus    
}

function set_cienv(){
    # Set other environment variables which were previously in the runner's toml
    # Skip image publishing and use a different naming convention in noCI mode
    if [ "$CI_JOB_NAME" != "noCI" ]; then
        CIENV_JOBDIR=/scratch/logs/CI-JOB-${CI_JOB_ID}_${CI_JOB_NAME} # previously in runner's toml
        CIENV_CVMFSVOLUME=/scratch/cvmfs_hep/CI-JOB-${CI_JOB_ID}_${CI_JOB_NAME} # previously in runner's toml
        CIENV_DOCKERREGISTRY=${CI_REGISTRY_IMAGE} # previously in runner's toml
        if [ "$CI_PROJECT_NAMESPACE" == "hep-benchmarks" ]; then
            CIENV_SINGULARITYREGISTRY="gitlab-registry.cern.ch/hep-benchmarks/hep-workloads-sif"
            CIENV_SINGULARITYROBOT="gitlab-ci-token"
            # BMK-1075 moving sif registry to gitlab-registry. 
            # The former registry.cern.ch remains for the time being for the old images
            #CIENV_SINGULARITYREGISTRY="registry.cern.ch/hep-workloads"
            #CIENV_SINGULARITYROBOT="robot-hep-workloads+gitlab"
        else
            CIENV_SINGULARITYREGISTRY="registry.cern.ch/${CI_PROJECT_NAMESPACE}" # for forks: e.g. registry.cern.ch/valassi (BMK-1002)
            CIENV_SINGULARITYROBOT="robot-${CI_PROJECT_NAMESPACE}+gitlab" # for forks: e.g. robot-valassi+gitlab (BMK-1000)
        fi
    else
        CIENV_JOBDIR=/tmp/${USER}/logs/noCI-${CI_JOB_ID}
        CIENV_CVMFSVOLUME=/tmp/${USER}/cvmfs_hep/noCI-${CI_JOB_ID}
        CIENV_DOCKERREGISTRY=""
        CIENV_SINGULARITYREGISTRY=""
        CIENV_SINGULARITYROBOT=""
    fi
    echo "CIENV_JOBDIR=$CIENV_JOBDIR"
    echo "CIENV_CVMFSVOLUME=$CIENV_CVMFSVOLUME"
    echo "CIENV_DOCKERREGISTRY=$CIENV_DOCKERREGISTRY"
    echo "CIENV_SINGULARITYREGISTRY=$CIENV_SINGULARITYREGISTRY"
    echo "CIENV_SINGULARITYROBOT=$CIENV_SINGULARITYROBOT"


    # Define a common singularity cache to speed up singularity runs (BMK-159)
    if [ "$SINGULARITY_CACHEDIR" != "" ] ; then
        CIENV_SINGULARITYCACHE=$SINGULARITY_CACHEDIR
    else
        if [ "$CI_JOB_NAME" != "noCI" ]; then
            CIENV_SINGULARITYCACHE=/scratch/singularity/cache
            CIENV_SINGULARITYTMP=/scratch/singularity/tmp  #where singularity will write temporary files in the build procedure
        else
            CIENV_SINGULARITYCACHE=/tmp/${USER}/singularity/cache
        fi
    fi
    echo "CIENV_SINGULARITYCACHE=$CIENV_SINGULARITYCACHE"

    # Move the singularity tmpdir away from /tmp to avoid disk space issues (BMK-1033)
    if [ "$SINGULARITY_TMPDIR" != "" ] ; then
        CIENV_SINGULARITYTMP=$SINGULARITY_TMPDIR
    else
        if [ "$CI_JOB_NAME" != "noCI" ]; then
            CIENV_SINGULARITYTMP=/scratch/singularity/tmp
        else
            CIENV_SINGULARITYTMP=/tmp/${USER}/singularity/tmp
        fi
    fi
    echo "CIENV_SINGULARITYTMP=$CIENV_SINGULARITYTMP"
}

##########################################################################
## block of functions to 
## Create temporary gitlab branch, open merge request, tag repository
##########################################################################

function check_http_response (){
    responsefile=$1
    HTTP_status_code=$(head -n1 ${responsefile} | cut -d ' ' -f2)
    if [ ${HTTP_status_code} -ge 200 ] && [ ${HTTP_status_code} -lt 300 ];
    then
        echo "[openMR] Successful curl request. HTTP status ${HTTP_status_code}"
        return 0
    else
        echo "[openMR] Failed curl request. HTTP status ${HTTP_status_code}"
        return 1
    fi
}

function openMR(){
    echo -e "\n@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
    echo -e "\n[utility.sh] ${FUNCNAME[0]} starting at $(date)\n"
    echo -e "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n"
    if [ "$CI_PROJECT_NAMESPACE" != "hep-benchmarks" ]; then
      echo "[openMR] CI_PROJECT_NAMESPACE is not hep-benchmarks. do not open a MR (BMK-1141)"
      return 0
    fi
    openMR_create_new_branch
    openMR_update_WL_list
    openMR_startMR
    openMR_tagRelease
    echo -e "\n@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
    echo -e "\n[utility.sh] ${FUNCNAME[0]} finished at $(date)\n"
    echo -e "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n"
}

function openMR_create_new_branch(){
    BODY="{
        \"branch\": \"${TMP_BRANCH}\",
        \"ref\": \"${CI_COMMIT_SHA}\"
    }";
    echo "[openMR] Creating new branch ${TMP_BRANCH} with BODY: $BODY"

    curl -i -X POST  "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/repository/branches" \
        --header "PRIVATE-TOKEN:${CI_PRIVATE_TOKEN}" \
        --header "Content-Type: application/json" \
        --data "${BODY}" > /tmp/out_curl.txt

    check_http_response /tmp/out_curl.txt || (echo "[openMR] Creation of the new branch failed. Exiting" ; cat /tmp/out_curl.txt; sleep 2 ; exit 1)
}

function openMR_update_WL_list(){
    echo "[openMR] update file WL_list.md"
    ls -l $CI_PROJECT_DIR/x86_image_folder_dump.txt
    #cat $CI_PROJECT_DIR/x86_image_folder_dump.txt
    total_size=$(cat $CI_PROJECT_DIR/x86_image_folder_dump.txt | grep -i total | tail -1 | awk '{print $1}')
    echo "[openMR] Retrieved container total size: ${total_size}"
    generate_wl_list ${total_size} > /WL_list.md
    cat /WL_list.md

    curl -i --request PUT \
        --header "PRIVATE-TOKEN:${CI_PRIVATE_TOKEN}" \
        -F "content=</WL_list.md" \
        -F "branch=${TMP_BRANCH}" \
        -F "commit_message=update WL list" \
        "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/repository/files/WL%5Flist%2Emd" > /tmp/out_curl.txt

    check_http_response /tmp/out_curl.txt || (echo "[openMR] update file WL_list.md. Exiting" ; cat /tmp/out_curl.txt ; sleep 2 ; exit 1)
}

function openMR_startMR(){
    BODY="{
        \"id\": ${CI_PROJECT_ID},
        \"source_branch\": \"${TMP_BRANCH}\",
        \"target_branch\": \"${TARGET_BRANCH}\",
        \"title\": \"merge ${CI_COMMIT_BRANCH} commit ${CI_COMMIT_SHORT_SHA} from ${CI_PIPELINE_ID}\",
        \"remove_source_branch\": true,
        \"allow_collaboration\": false
    }";
    echo "[openMR] Starting merge request with BODY: $BODY"
    curl -i -X POST "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/merge_requests" \
            --header "PRIVATE-TOKEN:${CI_PRIVATE_TOKEN}" \
            --header "Content-Type: application/json" \
            --data "${BODY}" > /tmp/out_curl.txt

    check_http_response /tmp/out_curl.txt || (echo "[openMR] Merge request failed. Exiting" ; cat /tmp/out_curl.txt ; sleep 2 ; exit 1)
}

function openMR_tagRelease(){
    echo "[openMR] Starting tag procedure"

    source_specfile || echo "[openMR] Cannot discover spec file. Skipping the git tag" 
    if [[ -n ${HEPWL_DOCKERIMAGENAME} ]] && [[ -n ${HEPWL_DOCKERIMAGETAG} ]] && [[ ${HEPWL_DOCKERIMAGETAG} =~ (^v.*$) ]]; then
        BODY="{
            \"tag_name\": \"v_${HEPWL_DOCKERIMAGENAME}_${HEPWL_DOCKERIMAGETAG}\",
            \"ref\": \"${CI_COMMIT_SHA}\"
        }";
        echo "[openMR] tagging repository with BODY: $BODY"

        curl -i -X POST  "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/repository/tags" \
            --header "PRIVATE-TOKEN:${CI_PRIVATE_TOKEN}" \
            --header "Content-Type: application/json" \
            --data "${BODY}" > /tmp/out_curl.txt

        check_http_response /tmp/out_curl.txt || (echo "[openMR] Tag request failed. Exiting" ; cat /tmp/out_curl.txt ; sleep 2 ; exit 1)
    else
        echo "[openMR] The image name:tag is not fully defined or does not start with v.*. Getting ${HEPWL_DOCKERIMAGENAME}:${HEPWL_DOCKERIMAGETAG}. Nothing to tag"
    fi
}

##########################################################################
## block of functions to 
## Create docker multiarch manifest
##########################################################################

function create_multiarch_docker_manifest(){
    echo -e "\n@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
    echo -e "\n[utility.sh] ${FUNCNAME[0]} starting at $(date)\n"
    echo -e "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n"
    source_specfile || fail "[create_multiarch_docker_manifest] Cannot discover spec file. Skipping the multiarch"
    IMAGE_LIST_TO_AMEND=""
    for somearch in ${HEPWL_BUILDARCH/,/ }; do 
        echo "[create_multiarch_docker_manifest] include image to amend for arch: $somearch"
        thetag=${HEPWL_DOCKERIMAGETAG}_${somearch} 
        theimage="${CI_REGISTRY_IMAGE}/${HEPWL_DOCKERIMAGENAME}:${thetag}"
        IMAGE_LIST_TO_AMEND="${IMAGE_LIST_TO_AMEND} --amend $theimage "
    done
    echo "[create_multiarch_docker_manifest] IMAGE_LIST_TO_AMEND=${IMAGE_LIST_TO_AMEND}"
    [[ "${IMAGE_LIST_TO_AMEND}" == "" ]] && fail "[create_multiarch_docker_manifest] no images to amend. Failing."
    mkdir ~/.docker
    echo "{\"experimental\":\"enabled\"}" > ~/.docker/config.json
    echo $CI_JOB_TOKEN | docker login -u gitlab-ci-token --password-stdin gitlab-registry.cern.ch || fail "failed login to docker registry"
    MULTIARCH_IMAGE=$CI_REGISTRY_IMAGE/${HEPWL_DOCKERIMAGENAME}:${HEPWL_DOCKERIMAGETAG}
    execute_command_retry docker manifest create ${MULTIARCH_IMAGE} "${IMAGE_LIST_TO_AMEND}" || fail "[create_multiarch_docker_manifest] "
    execute_command_retry docker manifest push --purge ${MULTIARCH_IMAGE} || fail "[create_multiarch_docker_manifest] "
    MULTIARCH_IMAGE=$CI_REGISTRY_IMAGE/${HEPWL_DOCKERIMAGENAME}:${HEPWL_DOCKERIMAGETAG} || fail "[create_multiarch_docker_manifest] "
    execute_command_retry docker manifest create ${MULTIARCH_IMAGE} "${IMAGE_LIST_TO_AMEND}" || fail "[create_multiarch_docker_manifest] "
    execute_command_retry docker manifest push --purge ${MULTIARCH_IMAGE} || fail "[create_multiarch_docker_manifest] "
    MULTIARCH_IMAGE=$CI_REGISTRY_IMAGE/${HEPWL_DOCKERIMAGENAME}:latest
    execute_command_retry docker manifest create ${MULTIARCH_IMAGE} "${IMAGE_LIST_TO_AMEND}" || fail "[create_multiarch_docker_manifest] "
    execute_command_retry docker manifest push --purge ${MULTIARCH_IMAGE} || fail "[create_multiarch_docker_manifest] "
    echo -e "\n@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
    echo -e "\n[utility.sh] ${FUNCNAME[0]} finished at $(date)\n"
    echo -e "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n"
}

##########################################################################
## block of functions to announce images
##########################################################################

# Announce the standalone HEP workload image with a local /cvmfs (BMK-80)
# (send an email from ${CI_MAIL_FROM} to ${CI_ANNOUNCE_TO} announcing a new image created in ${CI_JOB_URL})
# Input environment variable ${HEPWL_DOCKERIMAGENAME}: image name for the HEP workload
# Input environment variable ${HEPWL_DOCKERIMAGETAG}: image tag for the standalone HEP workload
# Input environment variable ${CIENV_DOCKERREGISTRY}: registry where the image should be pushed
# Input environment variables ${CI_MAIL_FROM}, ${CI_ANNOUNCE_TO} and ${CI_JOB_URL}
# Input environment variable ${HEPWL_BMKANNOUNCE}: if exists and "false", do not announce the release
function announce_standalone_image(){
    echo -e "\n@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
    echo -e "\n[utility.sh] ${FUNCNAME[0]} starting at $(date)\n"
    echo -e "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n"
    source_specfile || fail "[announce_standalone_image] Cannot discover spec file. Skipping the announcement"
    set_cienv
    echo "[announce_standalone_image] ................................................"
    echo "[announce_standalone_image] starting at $(date)"
    echo "[announce_standalone_image] current directory is $(pwd)"
    echo "[announce_standalone_image] send mail from CI_MAIL_FROM='${CI_MAIL_FROM}'"
    echo "[announce_standalone_image] send mail to CI_ANNOUNCE_TO='${CI_ANNOUNCE_TO}'"
    if [ "${CIENV_DOCKERREGISTRY}" == "" ]; then
        echo "[annnounce_standalone_image] WARNING: empty CIENV_DOCKERREGISTRY, nothing to do"
    else
        theimage="${CIENV_DOCKERREGISTRY}/${HEPWL_DOCKERIMAGENAME}:${HEPWL_DOCKERIMAGETAG}"
        echo "[announce_standalone_image] theimage: $theimage"
        if [ "${HEPWL_BMKANNOUNCE}" == "false" ]; then
            echo "[announce_standalone_image] WARNING! no mail sent, HEPWL_BMKANNOUNCE is '${HEPWL_BMKANNOUNCE}'"
        elif ! [[ ${HEPWL_DOCKERIMAGETAG} =~ ^v[0-9]*\.[0-9]*$ ]]; then # announce only images that respect the format v[0-9]*\.[0-9]* (BMK-147)
            echo "[announce_standalone_image] WARNING! no mail sent, tag ${HEPWL_DOCKERIMAGETAG} is not a vXX.YY release tag"
        elif [ "${CI_MAIL_FROM}" == "" ] || [ "${CI_ANNOUNCE_TO}" == "" ]; then # NB these variables are set only in the upstream repo, not in forks
            echo "[announce_standalone_image] WARNING! no mail sent, invalid CI_MAIL_FROM or CI_ANNOUNCE_TO"
        else
            postfix start
            announcement="announce.txt"
            echo -e "Dear HEP Benchmark developers, \n" > $announcement
            echo -e "we are pleased to inform that a new version has been released for the container image \n\n" >> $announcement
            echo -e "Docker: ${theimage}" >> $announcement
            echo -e "\nSingularity: \n " >> $announcement
            # SIF images still do not have a manifest catch all image
            for somearch in ${HEPWL_BUILDARCH/,/ }; do 
                echo -e "\t\t${CIENV_SINGULARITYREGISTRY}/${HEPWL_DOCKERIMAGENAME}:${HEPWL_DOCKERIMAGETAG}_${somearch}\n" >> $announcement
            done
            echo -e "COMMIT DESCRIPTION:\n${CI_COMMIT_DESCRIPTION}" >> $announcement
            echo -e "GitLab CI pipeline https://gitlab.cern.ch/hep-benchmarks/hep-workloads/-/pipelines/${CI_PIPELINE_ID}" >> $announcement
            echo -e "\nPlease DO NOT REPLY\nReport automatically generated from GitLab CI in job ${CI_JOB_URL}\n[$(date)]" >> $announcement
            echo -e "\nYours sincerely,\nHEPiX Benchmarking Working Group\n\n" >> $announcement
            cat $announcement
            cat $announcement | mail -r ${CI_MAIL_FROM} -s "New Docker container available $theimage" ${CI_ANNOUNCE_TO}
            sleep 100s # keep the container alive, otherwise no email is sent (emails are sent only once per minute, see BMK-80)
        fi
    fi
    echo "[announce_standalone_image] finished at $(date)"
    echo -e "\n@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
    echo -e "\n[utility.sh] ${FUNCNAME[0]} finished at $(date)\n"
    echo -e "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n"
    return 0
}

##########################################################################
## block of functions to 
## Test, build, publish Singularity images
##########################################################################

# Test standalone image in singularity
# Input environment variable ${HEPWL_DOCKERIMAGENAME}: image name for the HEP workload
# Input environment variable ${HEPWL_DOCKERIMAGETAG}: image tag for the standalone HEP workload
# Input environment variable ${CIENV_DOCKERREGISTRY}: registry where the image should be pushed
# Input environment variable ${HEPWL_BMKOS}: O/S for the HEP workload script
# Input environment variable ${HEPWL_BMKOPTS}: options for the HEP workload script
# Input environment variable ${HEPWL_BMKUSEGPU}: "1" if this is a GPU workload, "" otherwise
# Input environment variable ${CIENV_JOBDIR}/results: host directory where results should be stored
# Input environment variable ${CIENV_SINGULARITYCACHE}: singularity cache directory
# Output: status code for the success of the test and results in ${CIENV_JOBDIR}/results
# function test_standalone_image_singularity(){
#   echo "[test_standalone_image_singularity] ................................................"
#   echo "[test_standalone_image_singularity] starting at $(date)"
#   echo "[test_standalone_image_singularity] current directory is $(pwd)"
#   source_specfile || fail "[test_standalone_image_singularity] Cannot discover spec file. Failing"
#   set_cienv

#   if [ "$CI_JOB_NAME" == "" ] || [ "$CI_JOB_NAME" == "noCI" ]; then
#     echo "[test_standalone_image_singularity] skipping singularity test CI_JOB_NAME='$CI_JOB_NAME'"
#   elif [[ ${HEPWL_DOCKERIMAGETAG} =~ ^*NS$ ]]; then # for internal tests: skip singularity if tag ends by NS (No Singularity)
#     echo "[test_standalone_image_singularity] skipping singularity test HEPWL_DOCKERIMAGETAG='${HEPWL_DOCKERIMAGETAG}'"
#   else
#     echo "[test_standalone_image_singularity] executing singularity test for HEPWL_BMKOS='${HEPWL_BMKOS}'"
#     echo "[test_standalone_image_singularity] HEPWL_BMKUSEGPU=$HEPWL_BMKUSEGPU"
#     if [ "$HEPWL_BMKUSEGPU" == "1" ]; then USEGPU="--nv"; else USEGPU=""; fi
#     export SINGULARITY_CACHEDIR=$CIENV_SINGULARITYCACHE # use a common singularity cache to speed up singularity runs (BMK-159)
#     export SINGULARITY_TMPDIR=$CIENV_SINGULARITYTMP
#     echo "[test_standalone_image_singularity] SINGULARITY_CACHEDIR=${SINGULARITY_CACHEDIR} and SINGULARITY_TMPDIR=${SINGULARITY_TMPDIR}"
#     execute_command_retry singularity build ${HEPWL_DOCKERIMAGENAME}.sif docker-daemon://${CIENV_DOCKERREGISTRY}/${HEPWL_DOCKERIMAGENAME}:${HEPWL_DOCKERIMAGETAG} || fail "[test_standalone_image_singularity] failed the build of sif image"
#     echo -e "\n[test_standalone_image_singularity] Run WL in singularity (to test the image) via execute_command_retry - started"
#     execute_command_retry singularity run $USEGPU -B ${CIENV_JOBDIR}/results:/results ${HEPWL_DOCKERIMAGENAME}.sif $HEPWL_BMKOPTS -d
#     status=$?
#     echo -e "\n[test_standalone_image_singularity] Run WL in singularity (to test the image) via execute_command_retry - completed (status=$status)"
#     if [ $status -ne 0 ]; then rm -f ${HEPWL_DOCKERIMAGENAME}.sif; fail "[test_standalone_image_singularity] Run WL in singularity"; fi
#     echo "[test_standalone_image_singularity] validate json file" # see BMK-137
#     jsonFile=$(ls -1tr $CIENV_JOBDIR/results/*/${HEPWL_BMKDIR}_summary.json 1> >( head -1 ) ) || fail "[test_standalone_image_singularity] json summary file not found" # process substitution retains error code
#     validate_jsonfile $jsonFile || fail "[test_standalone_image_singularity] validate json file"
#     echo "[test_standalone_image_singularity] clean the singularity cache" # BMK-160
#     ls -ltr $SINGULARITY_CACHEDIR/oci-tmp/*/${HEPWL_DOCKERIMAGENAME}_test_ci_singularity.sif
#     sifs=$(ls -tr1 $SINGULARITY_CACHEDIR/oci-tmp/*/${HEPWL_DOCKERIMAGENAME}_test_ci_singularity.sif | head -n-1)
#     if [ "$sifs" == "" ]; then
#       echo "[test_standalone_image_singularity] no sif files to remove"
#     else
#       for sif in $sifs; do
#         echo "[test_standalone_image_singularity] remove $(dirname $sif)"
#         rm -rf $(dirname $sif)
#       done
#     fi

#   fi
#   echo "[test_standalone_image_singularity] finished at $(date)"
#   return 0
# }

function test_singularity_from_registry(){
    echo -e "\n@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
    echo -e "\n[utility.sh] ${FUNCNAME[0]} starting at $(date)\n"
    echo -e "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n"
    echo "[test_singularity_from_registry] ................................................"
    echo "[test_singularity_from_registry] starting at $(date)"
    source_specfile || fail "[test_singularity_from_registry] Cannot discover spec file. Failing"
    set_cienv
    if [ "$CI_JOB_NAME" == "" ] || [ "$CI_JOB_NAME" == "noCI" ]; then
        echo "[test_singularity_from_registry] skipping singularity test CI_JOB_NAME='$CI_JOB_NAME'"
        echo "[test_singularity_from_registry] finished at $(date)"
        return 0
    elif [[ ${HEPWL_DOCKERIMAGETAG} =~ ^*NS$ ]]; then # for internal tests: skip singularity if tag ends by NS (No Singularity)
        echo "[test_singularity_from_registry] skipping singularity test HEPWL_DOCKERIMAGETAG='${HEPWL_DOCKERIMAGETAG}'"
        echo "[test_singularity_from_registry] finished at $(date)"
        return 0
    else
        echo "[test_singularity_from_registry] current directory is $(pwd)"
    fi
    echo "[test_singularity_from_registry] executing singularity test for HEPWL_BMKOS='${HEPWL_BMKOS}'"
    echo "[test_singularity_from_registry] HEPWL_BMKUSEGPU=$HEPWL_BMKUSEGPU"
    if [ "$HEPWL_BMKUSEGPU" == "1" ]; then USEGPU="--nv"; else USEGPU=""; fi
    export SINGULARITY_CACHEDIR=$CIENV_SINGULARITYCACHE # use a common singularity cache to speed up singularity runs (BMK-159)
    export SINGULARITY_TMPDIR=$CIENV_SINGULARITYTMP
    echo "[test_singularity_from_registry] SINGULARITY_CACHEDIR=${SINGULARITY_CACHEDIR} and SINGULARITY_TMPDIR=${SINGULARITY_TMPDIR}"
    create_sing_cache_dirs
    thearch="$(uname -m)"
    if [[ $HEPWL_BUILDARCH =~ "$thearch" ]];
    then
        echo -e "\n[test_singularity_from_registry] WL is built for ${thearch}. Going to run the test"
    else
        echo -e "\n[test_singularity_from_registry] WL is not built for ${thearch}. Finishing without running" && return 0
    fi
    thetag=${HEPWL_DOCKERIMAGETAG}_"${thearch}"
    theimage=oras://${CIENV_SINGULARITYREGISTRY}/${HEPWL_DOCKERIMAGENAME}:${thetag}
    echo -e "\n[test_singularity_from_registry] Run WL in singularity to test the image ${theimage}"
    [ ! -e ${CIENV_JOBDIR}/results ] && mkdir -p ${CIENV_JOBDIR}/results && chmod a+rw ${CIENV_JOBDIR}/results
    execute_command_retry singularity run -e -C $USEGPU -B ${CIENV_JOBDIR}/results:/results ${theimage} $HEPWL_BMKOPTS -d
    status=$?
    echo -e "\n[test_singularity_from_registry] Run WL in singularity (to test the image) via execute_command_retry - completed (status=$status)"
    if [ $status -ne 0 ]; then 
        rm -f ${theimage}.sif; 
        fail "[test_singularity_from_registry] Run WL in singularity"; 
    fi
     execute_command_retry singularity run -e -C $USEGPU -B ${CIENV_JOBDIR}/results:/results ${theimage} $HEPWL_BMKOPTS -h > x86_image_folder_dump.txt
    echo "[test_singularity_from_registry] finished at $(date)"
    echo -e "\n@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
    echo -e "\n[utility.sh] ${FUNCNAME[0]} finished at $(date)\n"
    echo -e "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n"
    return 0
}

function publish_oras(){
    echo -e "\n@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
    echo -e "\n[utility.sh] ${FUNCNAME[0]} starting at $(date)\n"
    echo -e "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n"
    source_specfile || fail "[publish_oras] Cannot discover spec file. Failing" 
    set_cienv
    if [ "${CIENV_SINGULARITYREGISTRY}" == "" ]; then # BMK-1002
      echo "[publish_oras] WARNING: empty CIENV_SINGULARITYREGISTRY, no need to push to singularity registry"
      rerun 0
    elif [ "${CIENV_SINGULARITYROBOT}" == "" ]; then # BMK-1000
      echo "[publish_oras] WARNING: empty CIENV_SINGULARITYROBOT, will not push to singularity registry"
      return 0
    elif [ "${CI_SING_TOKEN}" == "" ]; then # BMK-1000
      echo "[publish_oras] WARNING: empty CI_SING_TOKEN, will not push to singularity registry"
      return 0
    else
      echo ""
    fi
   docker_to_sif
    echo -e "\n@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
    echo -e "\n[utility.sh] ${FUNCNAME[0]} finished at $(date)\n"
    echo -e "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n"
}

function docker_to_sif(){
    # Start procedure to download, build and push a singularity image
    # Using 
    #  - caches to speedup the process in case the cache is already populated (same runner as previous step)
    #  - oras to publish into registry

    export SINGULARITY_CACHEDIR=$CIENV_SINGULARITYCACHE
    export SINGULARITY_TMPDIR=$CIENV_SINGULARITYTMP
    create_sing_cache_dirs

    # BMK-1075 : moving sif registry back to gitlab-registry
    if [[ "${CIENV_SINGULARITYREGISTRY}" =~ (^gitlab-.*$) ]]; then
        (echo $CI_PRIVATE_TOKEN_SIF | singularity remote login -u ${CIENV_SINGULARITYROBOT} --password-stdin oras://gitlab-registry.cern.ch) || fail "[publish_oras.sh] cannot login to gitlab-registry.cern.ch"
    else
        # Maintain registry.cern.ch for legacy reasons
        (echo $CI_SING_TOKEN | singularity remote login -u ${CIENV_SINGULARITYROBOT} --password-stdin oras://registry.cern.ch) || fail "[publish_oras.sh] cannot login to ${CIENV_SINGULARITYREGISTRY}"
    fi

    # Loop on built architectures, as declared in HEPWL_BUILDARCH
    # and perform the build and  push
    echo "[publish_oras.sh] singularity build/publish procedure will be executed for architectures: ${HEPWL_BUILDARCH/,/ }"
    for somearch in ${HEPWL_BUILDARCH/,/ }; do 
        [[ "${CIENV_PROCESS_ARCH}" != "all" ]] && [[ "${somearch}" != "${CIENV_PROCESS_ARCH}" ]] && continue
        echo "[publish_oras.sh] singularity build/publish procedure for arch: $somearch"
        thetag=${HEPWL_DOCKERIMAGETAG}_${somearch} 
        execute_command_retry docker pull ${CIENV_DOCKERREGISTRY}/${HEPWL_DOCKERIMAGENAME}:${thetag} || fail "[publish_oras.sh] cannot pull docker image"
        
        # FOR DEBUG PURPOSES
        #CIENV_DOCKERREGISTRY=docker.io/library
        #HEPWL_DOCKERIMAGENAME=hello-world
        #thetag=latest
        
        execute_command_retry singularity build ${HEPWL_DOCKERIMAGENAME}.sif docker-daemon://${CIENV_DOCKERREGISTRY}/${HEPWL_DOCKERIMAGENAME}:${thetag} || fail "[publish_oras.sh] cannot build singularity image"

       execute_command_retry singularity push ${HEPWL_DOCKERIMAGENAME}.sif oras://${CIENV_SINGULARITYREGISTRY}/${HEPWL_DOCKERIMAGENAME}:${thetag} || fail "[publish_oras.sh] cannot push sif image"
        if [[ ${HEPWL_DOCKERIMAGETAG} =~ ^v[0-9]*\.[0-9]*$ ]]; then
            execute_command_retry singularity push ${HEPWL_DOCKERIMAGENAME}.sif oras://${CIENV_SINGULARITYREGISTRY}/${HEPWL_DOCKERIMAGENAME}:latest_${somearch} || fail "[publish_oras.sh] cannot push sif image"
        fi
        rm -f ${HEPWL_DOCKERIMAGENAME}.sif
    done
}

function create_sing_cache_dirs(){
    if [ ! -e "${SINGULARITY_CACHEDIR}" ]; then
        mkdir -p "${SINGULARITY_CACHEDIR}" || fail "[create_sing_cache_dirs] failed to create ${SINGULARITY_CACHEDIR}"
    fi
    if [ ! -e "${SINGULARITY_TMPDIR}" ]; then 
        mkdir -p "${SINGULARITY_TMPDIR}" || fail "[create_sing_cache_dirs] failed to create ${SINGULARITY_TMPDIR}"
    fi
}

function generate_wl_list(){
    set -x
    this_wl_size="$1"

    echo "# HEP Workloads list"
    echo '''

## To run

* Docker

```
docker run -v /path/to/local/folder:/results $DOCKER_IMAGE:latest
```
* Apptainer/Singularity

```
chmod a+rw /path/to/local/folder
apptainer run -B /path/to/local/folder:/results $SIF_IMAGE:latest_$arch
```
where:
 * arch=x86_64 or arch=aarch64
 * DOCKER_IMAGE and SIF_IMAGE path can be found in the below table, columns SIF/Docker image registry

'''

    LONG_STRING=""
    echo "| Experiment |  WL repo  | SIF image registry | Docker image registry| Latest Built Version | Latest Pipeline status | Unpacked container size | "
    echo "| -------- | -------- | -------- | -------- | -------- | -------- | -------- |"
    EXPERIMENTS=$(find . -mindepth 1 -maxdepth 1 -type d | sed -e 's@\.\/@@' | grep -E -v "common|build-executor|scripts|test|\..*" | sort )
    for aexp in ${EXPERIMENTS};
    do
        WORKLOADS=$(find "$aexp" -mindepth 1 -maxdepth 1 -type d| sed -e "s@$aexp\/@@" | sort)
        for wl in $WORKLOADS;
        do
            source "${aexp}/${wl}/${aexp}-${wl}.spec"
            pipeline_url=""
            wl_size=""
            if [ "${CI_COMMIT_BRANCH}" == "qa-build-${aexp}-${wl}" ]; then
                pipeline_url="https://gitlab.cern.ch/hep-benchmarks/hep-workloads/-/pipelines/${CI_PIPELINE_ID}"
                wl_size="${this_wl_size}"
            elif [ -f "${CI_PROJECT_DIR}"/WL_list.md ]; then
    	        pipeline_url=$( (grep "\[${aexp}_${wl}_pipelink\]:" "${CI_PROJECT_DIR}"/WL_list.md || echo ":") | cut -d ":" -f2- | sed -e 's@\s@@g')
                wl_size=$(cat "${CI_PROJECT_DIR}"/WL_list.md | grep "\[$wl\]\[${aexp}_${wl}_code\] " | awk -F'|' '{print $8}')
            fi
            
            echo "| $aexp | [$wl][${aexp}_${wl}_code] | [click for link][${aexp}_${wl}_sif] | [click for link][${aexp}_${wl}_img] | [${HEPWL_DOCKERIMAGETAG}][${aexp}_${wl}_pipelink] | ![ci][${aexp}_${wl}_pipeline]| ${wl_size} |"
            LONG_STRING="$LONG_STRING
                [${aexp}_${wl}_code]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads/-/blob/master/${aexp}/${wl}/${aexp}-${wl}
                [${aexp}_${wl}_sif]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads-sif/container_registry/?search%5B%5D=${aexp}-${wl}-bmk
                [${aexp}_${wl}_img]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads/container_registry/?search%5B%5D=${aexp}-${wl}-bmk
                [${aexp}_${wl}_pipelink]: ${pipeline_url}
                [${aexp}_${wl}_pipeline]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads/badges/qa-build-${aexp}-${wl}/pipeline.svg
                "
        done
    done

    echo -e "$LONG_STRING" | sed -e 's@^\s*@@g'
    set +x
}
