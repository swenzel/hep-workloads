#!/bin/bash

# Copyright 2019-2022 CERN. See the COPYRIGHT file at the top-level
# directory of this distribution. For licensing information, see the
# COPYING file at the top-level directory of this distribution.

###set -x # verbose

set -e # immediate exit on error

#------------------------------------------------------------------------------

# Get a CERN SSO cookie (from project HEP_OSlibs)
function get_gitlab_cookie()
{
  # Check arguments: expect "${ckf_var}"
  if [ "$1" == "" ] || [ "$2" != "" ]; then
    echo "PANIC! Wrong arguments to gitlab_token: \"$*\"" > /dev/stderr
    exit 1
  fi
  local _fva=$1

  # Get a CERN SSO cookie for the current Kerberos user against gitlab.cern.ch 
  if [ -d ~/private/ ]; then
    local _CKF=~/private/TMP-cookie.txt
    \rm -rf $_CKF
  else
    echo "WARNING! Directory ~/private/ does not exist: use /tmp" > /dev/stderr
    local _CKF=$(mktemp)
  fi
  # From https://linuxsoft.cern.ch/internal/repos/authz7-stable/x86_64/os/Packages/auth-get-sso-cookie-2.1.0-1.el7.noarch.rpm
  # See https://auth.docs.cern.ch/applications/command-line-tools/#auth-get-sso-token
  auth-get-sso-cookie -u https://gitlab.cern.ch/profile/account -o $_CKF
  echo "Retrieved cookie into $_CKF"
  eval ${_fva}=${_CKF}
}

#------------------------------------------------------------------------------

function usage(){
  echo "Usage: $0 [-n <jobnam (default:good_1)>] [-d] [-r1|-r2|-r3] <gitlabJobUrl>"
  echo "Example: $0 https://gitlab.cern.ch/valassi/hep-workloads/-/jobs/15427666 -d"
  echo "Example: $0 https://gitlab.cern.ch/valassi/hep-workloads/-/jobs/15398387 -d -n bad_1"
  echo ""
  echo "  -n <jobnam>        job directory name (default: good_1)"
  echo "  -d                 delete <bmkdir>/jobs/<jobnam>/* if directory exists"
  echo "  -r1 | -r2 | -r3    download the 1st (-r1: shrinkwrap, default), 2nd (-r2: docker) or 3rd (-r3: singularity) run"
  echo "  <gitlabJobUrl>     job url https://gitlab.cern.ch/<owner>/hep-workloads/-/jobs/<jobid>"
  echo ""
  exit 1
}

# Process input arguments
#--------------------------

deljob=
joburl=
jobnam=
rn=
while [ "$1" != "" ]; do
  if [ "$1" == "-n" ] && [ "$2" != "" ] && [ "$jobnam" == "" ]; then
    jobnam=$2; shift; shift
  elif [ "$1" == "-d" ] && [ "$deljob" == "" ]; then
    deljob=1; shift
  elif [ "$1" == "-r1" ] || [ "$1" == "-r2" ] || [ "$1" == "-r3" ]; then
    if [ "$rn" == "" ]; then rn=$1; shift; else usage; fi
  elif [ "${1##https://gitlab.cern.ch/}" != "$1" ] && [ "$joburl" == "" ]; then
    joburl=$1; shift
  else  
    usage
  fi
done
if [ "$jobnam" == "" ]; then jobnam="good_1"; fi
if [ "$rn" == "" ]; then rn=-r1; fi
if [ "$joburl" == "" ]; then usage; fi

echo "[$(basename $0)] deljob: ${deljob}"
echo "[$(basename $0)] joburl: ${joburl}"
echo "[$(basename $0)] jobnam: ${jobnam}"

# Get gitlab cookie
#--------------------------

get_gitlab_cookie CKF
if [ ! -f $CKF ]; then
  echo "ERROR! File $CKF not found"
  exit 1
fi

# Create tmp job dir
#--------------------------

scrdir=$(cd $(dirname $0); pwd)
tmpdir=$(mktemp -d)/jobs/${jobnam}
mkdir -p $tmpdir
cd $tmpdir
echo "[$(basename $0)] tmpdir: ${tmpdir}"

# Fetch gitlab artifacts
#--------------------------

echo ${joburl} > joburl.txt

set +e # NO immediate exit on error

# Keep both json (cvmfs-shrink and standalone), including sub-scores, and keep all Parsing messages
curl --fail -L --cookie $CKF --cookie-jar $CKF ${joburl}/raw | egrep '(Parsing results|"copies"|"score")' > cishortlog.txt
# Keep only the first (cvmfs-shrink) json, excluding sub-scores, and do not keep any Parsing messages
###curl --fail -L --cookie $CKF --cookie-jar $CKF ${joburl}/raw | egrep '"copies"' | head -1 > cishortlog.json
if [ "$?" != "0" ]; then echo "ERROR! could not fetch ${joburl}/raw"; exit 1; fi
echo "OK: fetched ${joburl}/raw"

curl --fail -L --cookie $CKF --cookie-jar $CKF ${joburl}/artifacts/download -odownload.zip
if [ "$?" != "0" ]; then echo "ERROR! could not fetch ${joburl}/artifacts/download"; exit 1; fi
echo "OK: fetched ${joburl}/artifacts/download"

set -e # immediate exit on error

# Delete gitlab cookie
#--------------------------

\rm -f $CKF

# Process gitlab artifacts
#--------------------------

unzip download.zip
\rm -f download.zip

tar -xzf results.tar.gz
\rm -f results.tar.gz

\mv scratch/logs/CI-JOB-*/results .
\rm -rf scratch

# Workaround for workloads like madgraph: drop the results/build directory if it exists
if [ -d results/build ]; then
  echo -e "\nWARNING! Remove directory $(pwd)/results/build from further processing"
  \rm -rf $(pwd)/results/build
fi

# If rn == -r1 (default), keep the first set of test results (cvmfs-shrink image)
# If rn == -r2, keep the second set of test results (standalone docker image)
# If rn == -r3, keep the third set of test results (standalone singularity image)
nresults=$(find results -mindepth 1 -maxdepth 1 -type d | wc -l)
echo -e "\nDirectory $(pwd)/results contains $nresults directories"
###find results -mindepth 1 -maxdepth 1 -type d # FOR DEBUGGING
if [ "$nresults" == "0" ]; then
  echo -e "\nERROR! Directory $(pwd)/results should contain at least one directory\n"
  exit 1
elif [ "$nresults" == "1" ]; then
  # If only one set of test results is found, the job failed (but results can be retrieved anyway)
  echo -e "\nWARNING! Directory $(pwd)/results contains less than two directories\n"
  if [ "$rn" != "-r1" ]; then echo -e "ERROR! $rn cannot be used in this case"; exit 1; fi
elif [ "$nresults" == "2" ]; then
  # If only two sets of test results are found, the singularity test failed or was skipped
  echo -e "\nWARNING! Directory $(pwd)/results contains less than three directories\n"
  if [ "$rn" == "-r3" ]; then echo -e "ERROR! $rn cannot be used in this case"; exit 1; fi
elif [ "$nresults" != "3" ] && [ "$nresults" != "3" ]; then
  echo -e "\nWARNING! Directory $(pwd)/results contains more than three directories???\n"
fi
if [ "$rn" == "-r1" ]; then
  testn="$(find results -mindepth 1 -maxdepth 1 -type d | sort | head -1)" # test1
elif [ "$rn" == "-r2" ]; then
  testn="$(find results -mindepth 1 -maxdepth 1 -type d | sort | head -2 | tail -1)" # test2
elif [ "$rn" == "-r3" ]; then
  testn="$(find results -mindepth 1 -maxdepth 1 -type d | sort | head -3 | tail -1)" # test3
fi
\mv $testn/* . # move everything including archive_processes_logs.tgz that contains $testn/proc_*
logarch=archive_processes_logs.tgz 
if [ ! -f $logarch ]; then
  echo -e "\nERROR! $logarch not found in $(pwd)\n"
  exit 1
else
  echo -e "\nOK: $logarch found in $(pwd)\n"
fi
tar -xvzf $logarch # this contains $testn/proc_*
\rm -f $logarch
for procdir in $testn/proc_*; do
  mkdir $(basename $procdir)
  \mv $procdir/*log $(basename $procdir)/. # move $testn/proc_*/*.log
done
if ls results/cvmfs*.spec.txt &> /dev/null; then
  \mv results/cvmfs*.spec.txt .
fi
\rm -rf results

# Identify the bmk
#--------------------------

bmkspecs=$(cd $scrdir/..; ls */*/*.spec)
bmknam=
for bmkspec in $bmkspecs; do
  if [ -f $(basename $bmkspec .spec)_summary.json ]; then
    bmknam=$(basename $bmkspec .spec)
    bmkdir=$(dirname $bmkspec)/$bmknam
    break
  fi
done

if [ "${bmknam}" == "" ]; then
  echo -e "\nERROR! Could not identify benchmark from json files" *_summary.json
  exit 1
fi

jsonsummary=${bmknam}_summary.json
ln -sf ${jsonsummary} summary.json

# Move to job dir
#--------------------------

jobdir=${scrdir}/../${bmkdir}/jobs/${jobnam}
echo "[$(basename $0)] jobdir: ${jobdir}"
if [ -d ${jobdir} ]; then
  if [ "${deljob}" == "1" ]; then
    echo -e "\nWARNING! Directory ${jobdir} exists: delete all its contents"
    \rm -rf ${jobdir}/*
  else
    echo -e "\nERROR! Directory ${jobdir} exists: use option -d to delete all its contents"
    exit 1
  fi
else
  mkdir -p ${jobdir}
fi
cd ${jobdir}
mv ${tmpdir}/* .

# Remove these files as they are not needed and are overwritten in local parser tests (BMK-1009)
\rm -f version_derived.json parser_output.json

# Dump results
#--------------------------

echo -e "\n[$(basename $0)] Retrieved new reference results from ${joburl} into $(pwd)\n"
ls -lR

echo -e "\n[$(basename $0)] cishortlog.txt:\n"
cat cishortlog.txt

echo -e "\n[$(basename $0)] ${jsonsummary}:\n"
cat ${jsonsummary}
echo ""
