#!/bin/bash

# Copyright 2019-2023 CERN. See the COPYRIGHT file at the top-level
# directory of this distribution. For licensing information, see the
# COPYING file at the top-level directory of this distribution.

function usage(){
  echo "Usage: $(basename $0) [-r regex|--all|--days]"
  exit 1
}

quiet=0
if [ "$1" == "-q" ]; then
  quiet=1
  shift
fi

if [ "$1" == "" ]; then
  # Remove all <none> images
  # In the clean stage of .gitlab-ci.sh, this also removes newly created layers, preventing caching across CI runs (BMK-1010)
  IMAGES=$(docker images | grep '<none>' | awk '{print $3}')
elif [ "$1" == "--days" ]; then
  # Remove all <none> images created more than 2 days ago
  # In the clean stage of .gitlab-ci.sh, this does NOT remove newly created layers, easing caching across CI runs (BMK-1010)
  IMAGES_A=$(docker images | grep '<none>' | egrep -v '(second|minute|hour)(s|) ago' | awk '{print $3}')
  # Remove also images that are not <none>, if they are larger than 1 GB, except for builder images (BMK-1157)
  IMAGES_B=$(docker images | grep -e "ago *[0-9\.]*GB" | grep -v hep-workload-builder | awk '{print $3}')
  if [ "$IMAGES_A" != "" ]; then IMAGES=$(echo ${IMAGES_A} " " ${IMAGES_B}); else IMAGES=${IMAGES_B}; fi
elif [ "$1" == "--all" ]; then
  IMAGES=$(docker images | egrep -v '^(REPOSITORY|gitlab-registry.cern.ch/linuxsupport/(slc6|cc7)-base)' | awk '{print $3}')
  shift
elif [ "$1" == "-r" ] && [ "$2" != "" ] ; then
  # use regular expression passed by $2
  IMAGES=$(docker images | egrep -v '^REPOSITORY' | egrep "$2" | awk '{print $3}')
  shift
  shift
else
  usage
fi

###echo $IMAGES

if [ "$quiet" == "0" ]; then
  docker images
  echo ""
fi

if [ "$IMAGES" != "" ]; then
  echo Clearing images "$IMAGES"
  docker rmi -f $IMAGES
  if [ "$quiet" == "0" ]; then
    echo ""
    docker images
  fi
else
  echo There are no docker images to clear
fi

