# Changelog for Belle2 gen sim reco MultiArchitecture (MA)

## [Unreleased]

## [v2.2] 2023-11-06
- Limit the number of loaded cores using the argument -n | --ncores

## [v2.1] 2023-02-01
- include prmon fix for ARM

## [v2.0] 2023-01-30
- First working release of Belle2 gen-sim-reco built for x86_64 and aarch64
- The version starts from v2.0 to avoid confusion with the legacy images built in belle2/gen-sim-reco

